import React from 'react';

const Button = ({text, backgroundColor}) => {
    return (
        <>
            <button style={backgroundColor && { backgroundColor }}>{text}</button>
        </>
    );
};

export default Button;
