import React from 'react';
import CheckButton from './index';

export default {
    title: 'Check Button',
    component: CheckButton,
    argTypes: {
        backgroundColor: { control: 'color' },
    },
};

const Template = (args) => <CheckButton {...args} />;

export const Primary = Template.bind({});

Primary.args = {
    text: 'Boton'
};